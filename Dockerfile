FROM tomcat:latest
ARG rel_n
COPY ./ROOT.xml ./conf/Catalina/localhost/ROOT.xml
COPY ./build/libs/language-ritual-${rel_n}.war ./webapps/language-ritual.war
CMD ["catalina.sh", "run"]

EXPOSE 8080