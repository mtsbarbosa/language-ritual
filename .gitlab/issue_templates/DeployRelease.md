## 1. Description
Execute sh command and go ahead step by step
This ticket should be closed only once release is alive in PROD

X.X.X = release to be deployed

--srv if it is supposed to deploy server  
--clt if it is supposed to deploy client
--prd if it to PROD, otherwise it will point to qa
--mig if it is necessary to migrate

## 2. Progress
| Task | QA | PROD |
| ------ | ------ | ------ |
| Execute   sudo sh deploy.sh X.X.X [--srv] [--clt] [--prd] [--mig]|  |  |

## 3. Other details