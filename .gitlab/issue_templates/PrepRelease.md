## 1. Description
Execute sh command and go ahead step by step
This ticket should be closed only once release is alive in PROD

Dont skip checking for conflicts in your ui before advancing steps

## 2. Progress
| Task | QA | PROD |
| ------ | ------ | ------ |
| Execute prepare_?ENV?.sh X.X.X |  |  |

## 3. Other details