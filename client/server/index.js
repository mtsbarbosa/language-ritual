const express = require('express')
const consola = require('consola')
const { Nuxt, Builder } = require('nuxt')
const app = express()

// Import and Set Nuxt.js options
let config = require('../nuxt.config.js')
config.dev = !(process.env.NODE_ENV === 'production')

async function start() {
  // Init Nuxt.js
  const nuxt = new Nuxt(config)

  let host = '127.0.0.1';
  if(process.env.NODE_ENV === 'production'){
    host = process.env.HOST || '0.0.0.0';
  }
  let port = process.env.PORT || 3000;

  // Build only in dev mode
  const builder = new Builder(nuxt)
  await builder.build()

  // Give nuxt middleware to express
  app.use(nuxt.render)

  // Listen the server
  app.listen(port, host)
  consola.ready({
    message: `Server listening on http://${host}:${port}`,
    badge: true
  })
}
start()
