import Cookies from 'cookie-universal';

function createCookie({req, res}) {
  return Cookies(req, res);
}

export default function (context) {
  const cookies = createCookie(context);
  const session = cookies.get('JSESSIONID');
  if(process.server) {
    if (!session) {
      context.redirect('/login');
    } else if (context.route.path === '/' || context.route.path === '/login') {
      context.redirect('/management/language/list');
    }
  }
}
