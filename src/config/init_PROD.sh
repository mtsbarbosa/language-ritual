#!/usr/bin/env bash
#~/etc/init_QA.sh
clear
sudo apt-get update
echo "Installing curl"
sudo apt-get install curl
echo "Installing git"
sudo apt-get install git
echo "Installing jdk"
sudo apt-get install default-jdk
echo "Installing Docker"
sudo apt-get install apt-transport-https ca-certificates software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(sudo lsb_release -cs) stable"
sudo apt-get update
sudo apt-get install docker-ce
echo "Installing docker-compose"
sudo curl -L "https://github.com/docker/compose/releases/download/1.23.2/docker-compose-$(sudo uname -s)-$(sudo uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
#echo "Adding group tomcat"
#sudo groupadd tomcat
#sudo chmod 777 -R /opt
#sudo useradd -s /bin/false -g tomcat -d /opt/tomcat tomcat
#echo "Downloading tomcat"
#cd /tmp
#curl -O https://www-us.apache.org/dist/tomcat/tomcat-9/v9.0.16/bin/apache-tomcat-9.0.16.tar.gz
#echo "Creating tomcat folder"
#sudo mkdir /opt/tomcat
#sudo tar xzvf apache-tomcat-9*tar.gz -C /opt/tomcat --strip-components=1
#sudo ln -s /opt/tomcat/apache-tomcat-9.0.16 /opt/tomcat/latest
#cd /opt/tomcat
#sudo chgrp -R tomcat /opt/tomcat
#sudo chmod -R g+r conf
#sudo chmod g+x conf
#sudo chown -R tomcat webapps/ work/ temp/ logs/
#sudo chown -RH tomcat: /opt/tomcat/latest
#sudo sh -c 'chmod +x /opt/tomcat/latest/bin/*.sh'
#echo "Setting up service"
#sudo echo $'[Unit]\nDescription=Tomcat 9 servlet container\nAfter=network.target\n\n[Service]\nType=forking\n\nUser=tomcat\nGroup=tomcat\n\nEnvironment="JAVA_HOME=/usr/lib/jvm/default-java"\nEnvironment="JAVA_OPTS=-Djava.security.egd=file:///dev/urandom -Djava.awt.headless=true"\n\nEnvironment="CATALINA_BASE=/opt/tomcat/latest"\nEnvironment="CATALINA_HOME=/opt/tomcat/latest"\nEnvironment="CATALINA_PID=/opt/tomcat/latest/temp/tomcat.pid"\nEnvironment="CATALINA_OPTS=-Xms512M -Xmx1024M -server -XX:+UseParallelGC"\n\nExecStart=/opt/tomcat/latest/bin/startup.sh\nExecStop=/opt/tomcat/latest/bin/shutdown.sh\n\n[Install]\nWantedBy=multi-user.target' > /etc/systemd/system/tomcat.service
#sudo systemctl daemon-reload
echo "Creating /etc/server folder"
sudo mkdir -p /etc/server
sudo chmod -R 777 /etc/server
cd /etc/server
echo "Cloning repository"
git clone "https://gitlab.com/mtsbarbosa/language-ritual.git"
cd language-ritual
git checkout tags/$1
git pull




