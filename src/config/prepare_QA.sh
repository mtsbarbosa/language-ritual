#!/usr/bin/env bash
#~/etc/prepare_QA.sh
clear
echo "Creating qa-release-$1 release branch"
git checkout develop
git pull
git checkout qa
git pull
git checkout -b "qa-release-$1"
git pull
#echo "Changing gradle version on qa-release-$1 to $1"
#./gradlew -PprojVersion=$1 build 
echo "Merging develop to qa-release-$1"
git checkout "qa-release-$1"
git pull
git merge develop
read -p "Solve conflicts, commit if necessary. Done?(press any key)" fwd
git push --set-upstream origin "qa-release-$1"
echo "Merging qa-release-$1 to qa"
git checkout qa
git pull
git merge "qa-release-$1"
read -p "Solve conflicts, commit if necessary. Done?(press any key)" fwd
git push
echo "SUCCESS"




