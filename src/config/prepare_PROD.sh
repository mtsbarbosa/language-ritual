#!/usr/bin/env bash
#~/etc/prepare_PROD.sh
clear
echo "Creating prod-release-$1 release branch"
git checkout qa
git pull
git checkout master
git pull
git checkout -b "prod-release-$1"
git pull
#echo "Changing gradle version on qa-release-$1 to $1"
#./gradlew -PprojVersion=$1 build 
echo "Merging qa to prod-release-$1"
git checkout "prod-release-$1"
git pull
git merge qa
read -p "Solve conflicts, commit if necessary. Done?(press any key)" fwd
git push --set-upstream origin "prod-release-$1"
echo "Merging prod-release-$1 to master"
git checkout master
git pull
git merge "prod-release-$1"
read -p "Solve conflicts, commit if necessary. Done?(press any key)" fwd
git push
echo "Creating tag release-$1"
git tag "release-$1"
git push --tags
echo "SUCCESS"




