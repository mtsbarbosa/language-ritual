max_bkps=10;
n_bkps=$(find /var/lib/cassandra/snapshots -mindepth 1 -maxdepth 1 -type d | wc -l);
echo "number of snapshot folders found: $n_bkps";
if [ "$n_bkps" -ge "$max_bkps" ]; then
	read -p "maximum number of backups ($max_bkps) reached, deleting last one. Are you sure?(press any key)" fwd;
	last_folder=$(ls /var/lib/cassandra/snapshots -1 | head -1);
	read -p "deleting folder $last_folder. Are you sure?(press any key)" fwd;
	rm -rf "/var/lib/cassandra/snapshots/$last_folder";
	echo "$last_folder was deleted";
fi

echo "executing snapshot script on container";
docker exec -it $1 nodetool snapshot -t $2;

echo "creating /var/lib/cassandra/snapshots/$2";
mkdir "/var/lib/cassandra/snapshots/$2";
for f in $(docker exec -it $1 ls /var/lib/cassandra/data/languageritual);
do
    file_name=$(echo "$f" | sed 's/[^a-zA-Z0-9-]//g');
	echo "Starting on $file_name";
	echo "creating /var/lib/cassandra/snapshots/$2/$file_name";
	mkdir /var/lib/cassandra/snapshots/"$2"/"$file_name";
	echo "copying file from docker container";
	docker cp "$1":/var/lib/cassandra/data/languageritual/"$file_name"/snapshots/"$2"/. /var/lib/cassandra/snapshots/"$2"/"$file_name";

done;

echo "clearing snapshots inside container volume";
docker exec -it $1 nodetool -h localhost -p 7199 clearsnapshot;

echo "Backup finished";
