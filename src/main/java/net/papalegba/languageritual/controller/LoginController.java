package net.papalegba.languageritual.controller;

import net.papalegba.languageritual.util.SessionUtil;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoginController {
	@RequestMapping("/login")
	public ResponseEntity<String> login() {
		SessionUtil.getSession(true);
		return new ResponseEntity<>(HttpStatus.OK);
	}
}
