package net.papalegba.languageritual.controller;

import net.papalegba.languageritual.model.Action;
import net.papalegba.languageritual.model.ActionObjekt;
import net.papalegba.languageritual.model.Objekt;
import net.papalegba.languageritual.model.ObjektConnection;
import net.papalegba.languageritual.model.Subject;
import net.papalegba.languageritual.model.SubjectAction;
import net.papalegba.languageritual.service.ActionObjektService;
import net.papalegba.languageritual.service.ActionService;
import net.papalegba.languageritual.service.ObjektConnectionService;
import net.papalegba.languageritual.service.ObjektService;
import net.papalegba.languageritual.service.SubjectActionService;
import net.papalegba.languageritual.service.SubjectService;
import net.papalegba.languageritual.vo.ActionObjektResponseVO;
import net.papalegba.languageritual.vo.SubjectActionResponseVO;
import net.papalegba.languageritual.vo.ThermVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.UUID;

@RestController
public class LanguageController {
	@Autowired
	private SubjectService subjectService;
	@Autowired
	private ActionService actionService;
	@Autowired
	private ObjektConnectionService objektConnectionService;
	@Autowired
	private ObjektService objektService;
	@Autowired
	private SubjectActionService subjectActionService;
	@Autowired
	private ActionObjektService actionObjektService;

	@RequestMapping(value = "/languages/{id}/subjects", method = RequestMethod.GET)
	public List<Subject> getSubjects(@PathVariable String id, @RequestParam("q") Optional<String> q){
		if(!q.isPresent() || StringUtils.isEmpty(q.get())) {
			return subjectService.findByLanguageId(UUID.fromString(id));
		}
		return subjectService.findByTranslationContainingAndLanguageId(q.get(), UUID.fromString(id));
	}

	@RequestMapping(value = "/languages/{id}/subjects", method = RequestMethod.POST)
	public Subject postSubject(@PathVariable String id, @RequestBody Subject subject){
		subject.setLanguageId(UUID.fromString(id));
		subject.setId(UUID.randomUUID());
		return subjectService.insert(subject);
	}

	@RequestMapping(value = "/languages/{id}/subjects/{sub_id}", method = RequestMethod.PUT)
	public Subject putSubject(@PathVariable String id, @PathVariable String sub_id, @RequestBody Subject subject){
		subject.setLanguageId(UUID.fromString(id));
		subject.setId(UUID.fromString(sub_id));
		return subjectService.save(subject);
	}

	@RequestMapping(value = "/languages/{id}/subjects/{sub_id}", method = RequestMethod.DELETE)
	public ResponseEntity<String> deleteSubject(@PathVariable String id, @PathVariable String sub_id) {
		try {
			subjectService.deleteById(UUID.fromString(sub_id));
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}catch (Exception e){
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/languages/{id}/actions", method = RequestMethod.GET)
	public List<Action> getActions(@PathVariable String id, @RequestParam("q") Optional<String> q){
		if(!q.isPresent() || StringUtils.isEmpty(q.get())) {
			return actionService.findByLanguageId(UUID.fromString(id));
		}
		return actionService.findByTranslationContainingAndLanguageId(q.get(), UUID.fromString(id));
	}

	@RequestMapping(value = "/languages/{id}/actions", method = RequestMethod.POST)
	public Action postAction(@PathVariable String id, @RequestBody Action action){
		action.setLanguageId(UUID.fromString(id));
		action.setId(UUID.randomUUID());
		return actionService.insert(action);
	}

	@RequestMapping(value = "/languages/{id}/actions/{sub_id}", method = RequestMethod.PUT)
	public Action putAction(@PathVariable String id, @PathVariable String sub_id, @RequestBody Action action){
		action.setLanguageId(UUID.fromString(id));
		action.setId(UUID.fromString(sub_id));
		return actionService.save(action);
	}

	@RequestMapping(value = "/languages/{id}/actions/{sub_id}", method = RequestMethod.DELETE)
	public ResponseEntity<String> deleteAction(@PathVariable String id, @PathVariable String sub_id) {
		try {
			actionService.deleteById(UUID.fromString(sub_id));
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}catch (Exception e){
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/languages/{id}/objektconnections", method = RequestMethod.GET)
	public List<ObjektConnection> getObjektConnections(@PathVariable String id, @RequestParam("q") Optional<String> q){
		if(!q.isPresent() || StringUtils.isEmpty(q.get())) {
			return objektConnectionService.findByLanguageId(UUID.fromString(id));
		}
		return objektConnectionService.findByTranslationContainingAndLanguageId(q.get(), UUID.fromString(id));
	}

	@RequestMapping(value = "/languages/{id}/objektconnections", method = RequestMethod.POST)
	public ObjektConnection postObjektConnection(@PathVariable String id, @RequestBody ObjektConnection objektConnection){
		objektConnection.setLanguageId(UUID.fromString(id));
		objektConnection.setId(UUID.randomUUID());
		return objektConnectionService.insert(objektConnection);
	}

	@RequestMapping(value = "/languages/{id}/objektconnections/{sub_id}", method = RequestMethod.PUT)
	public ObjektConnection putObjektConnection(@PathVariable String id, @PathVariable String sub_id, @RequestBody ObjektConnection objektConnection){
		objektConnection.setLanguageId(UUID.fromString(id));
		objektConnection.setId(UUID.fromString(sub_id));
		return objektConnectionService.save(objektConnection);
	}

	@RequestMapping(value = "/languages/{id}/objektconnections/{sub_id}", method = RequestMethod.DELETE)
	public ResponseEntity<String> deleteObjektConnection(@PathVariable String id, @PathVariable String sub_id) {
		try {
			objektConnectionService.deleteById(UUID.fromString(sub_id));
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}catch (Exception e){
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/languages/{id}/objekts", method = RequestMethod.GET)
	public List<Objekt> getObjekts(@PathVariable String id, @RequestParam("q") Optional<String> q){
		if(!q.isPresent() || StringUtils.isEmpty(q.get())) {
			return objektService.findByLanguageId(UUID.fromString(id));
		}
		return objektService.findByTranslationContainingAndLanguageId(q.get(), UUID.fromString(id));
	}

	@RequestMapping(value = "/languages/{id}/objekts", method = RequestMethod.POST)
	public Objekt postObjekt(@PathVariable String id, @RequestBody Objekt objekt){
		objekt.setLanguageId(UUID.fromString(id));
		objekt.setId(UUID.randomUUID());
		return objektService.insert(objekt);
	}

	@RequestMapping(value = "/languages/{id}/objekts/{sub_id}", method = RequestMethod.PUT)
	public Objekt putObjekt(@PathVariable String id, @PathVariable String sub_id, @RequestBody Objekt objekt){
		objekt.setLanguageId(UUID.fromString(id));
		objekt.setId(UUID.fromString(sub_id));
		return objektService.save(objekt);
	}

	@RequestMapping(value = "/languages/{id}/objekts/{sub_id}", method = RequestMethod.DELETE)
	public ResponseEntity<String> deleteObjekt(@PathVariable String id, @PathVariable String sub_id) {
		try {
			objektService.deleteById(UUID.fromString(sub_id));
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}catch (Exception e){
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/languages/{id}/subjectactions", method = RequestMethod.GET)
	public List<SubjectActionResponseVO> getSubjectActions(@PathVariable String id){
		List<SubjectAction> subjectAction = subjectActionService.findByLanguageId(UUID.fromString(id));
		List<SubjectActionResponseVO> response = new ArrayList<>();
		subjectAction.forEach(sa -> {
			SubjectActionResponseVO saResp = new SubjectActionResponseVO();
			saResp.setId(sa.getId());
			saResp.setSubjectId(sa.getSubjectId());
			saResp.setActionId(sa.getActionId());
			saResp.setSubject(subjectService.findById(sa.getSubjectId()).get());
			saResp.setAction(actionService.findById(sa.getActionId()).get());
			response.add(saResp);
		});
		return response;
	}

	@RequestMapping(value = "/languages/{id}/subjectactions", method = RequestMethod.POST)
	public SubjectActionResponseVO postSubjectAction(@PathVariable String id, @RequestBody SubjectAction subjectAction){
		subjectAction.setLanguageId(UUID.fromString(id));
		subjectAction.setId(UUID.randomUUID());
		subjectAction = subjectActionService.insert(subjectAction);
		SubjectActionResponseVO response = new SubjectActionResponseVO();
		response.setId(subjectAction.getId());
		response.setSubjectId(subjectAction.getSubjectId());
		response.setActionId(subjectAction.getActionId());
		response.setSubject(subjectService.findById(subjectAction.getSubjectId()).get());
		response.setAction(actionService.findById(subjectAction.getActionId()).get());
		return response;
	}

	@RequestMapping(value = "/languages/{id}/subjectactions/{sub_id}", method = RequestMethod.PUT)
	public SubjectActionResponseVO putSubjectAction(@PathVariable String id, @PathVariable String sub_id, @RequestBody SubjectAction subjectAction){
		subjectAction.setLanguageId(UUID.fromString(id));
		subjectAction.setId(UUID.fromString(sub_id));
		subjectAction = subjectActionService.save(subjectAction);
		SubjectActionResponseVO response = new SubjectActionResponseVO();
		response.setId(subjectAction.getId());
		response.setSubjectId(subjectAction.getSubjectId());
		response.setActionId(subjectAction.getActionId());
		response.setSubject(subjectService.findById(subjectAction.getSubjectId()).get());
		response.setAction(actionService.findById(subjectAction.getActionId()).get());
		return response;
	}

	@RequestMapping(value = "/languages/{id}/subjectactions/{sub_id}", method = RequestMethod.DELETE)
	public ResponseEntity<String> deleteSubjectAction(@PathVariable String id, @PathVariable String sub_id) {
		try {
			subjectActionService.deleteById(UUID.fromString(sub_id));
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}catch (Exception e){
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/languages/{id}/actionobjekts", method = RequestMethod.GET)
	public List<ActionObjektResponseVO> getActionObjekts(@PathVariable String id){
		List<ActionObjekt> actionObjekts = actionObjektService.findByLanguageId(UUID.fromString(id));
		List<ActionObjektResponseVO> response = new ArrayList<>();
		actionObjekts.forEach(ao -> {
			ActionObjektResponseVO aoResp = new ActionObjektResponseVO();
			aoResp.setId(ao.getId());
			aoResp.setActionId(ao.getActionId());
			aoResp.setObjektconnectionId(ao.getObjektconnectionId());
			aoResp.setObjektId(ao.getObjektId());
			aoResp.setAction(actionService.findById(ao.getActionId()).get());
			if(ao.getObjektconnectionId() != null) {
				aoResp.setObjektConnection(objektConnectionService.findById(ao.getObjektconnectionId()).get());
			}
			aoResp.setObjekt(objektService.findById(ao.getObjektId()).get());
			response.add(aoResp);
		});
		return response;
	}

	@RequestMapping(value = "/languages/{id}/actionobjekts", method = RequestMethod.POST)
	public ActionObjektResponseVO postActionObjekt(@PathVariable String id, @RequestBody ActionObjekt actionObjekt){
		actionObjekt.setLanguageId(UUID.fromString(id));
		actionObjekt.setId(UUID.randomUUID());
		actionObjekt = actionObjektService.insert(actionObjekt);
		ActionObjektResponseVO response = new ActionObjektResponseVO();
		response.setId(actionObjekt.getId());
		response.setActionId(actionObjekt.getActionId());
		response.setObjektconnectionId(actionObjekt.getObjektconnectionId());
		response.setObjektId(actionObjekt.getObjektId());
		response.setAction(actionService.findById(actionObjekt.getActionId()).get());
		if(actionObjekt.getObjektconnectionId() != null) {
			response.setObjektConnection(objektConnectionService.findById(actionObjekt.getObjektconnectionId()).get());
		}
		response.setObjekt(objektService.findById(actionObjekt.getObjektId()).get());
		return response;
	}

	@RequestMapping(value = "/languages/{id}/actionobjekts/{sub_id}", method = RequestMethod.PUT)
	public ActionObjektResponseVO putActionObjekt(@PathVariable String id, @PathVariable String sub_id, @RequestBody ActionObjekt actionObjekt){
		actionObjekt.setLanguageId(UUID.fromString(id));
		actionObjekt.setId(UUID.fromString(sub_id));
		actionObjekt = actionObjektService.save(actionObjekt);
		ActionObjektResponseVO response = new ActionObjektResponseVO();
		response.setId(actionObjekt.getId());
		response.setActionId(actionObjekt.getActionId());
		response.setObjektconnectionId(actionObjekt.getObjektconnectionId());
		response.setObjektId(actionObjekt.getObjektId());
		response.setAction(actionService.findById(actionObjekt.getActionId()).get());
		if(actionObjekt.getObjektconnectionId() != null) {
			response.setObjektConnection(objektConnectionService.findById(actionObjekt.getObjektconnectionId()).get());
		}
		response.setObjekt(objektService.findById(actionObjekt.getObjektId()).get());
		return response;
	}

	@RequestMapping(value = "/languages/{id}/actionobjekts/{sub_id}", method = RequestMethod.DELETE)
	public ResponseEntity<String> deleteActionObjekt(@PathVariable String id, @PathVariable String sub_id) {
		try {
			actionObjektService.deleteById(UUID.fromString(sub_id));
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}catch (Exception e){
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/languages/{id}/generate", method = RequestMethod.GET)
	public ResponseEntity<?> generate(@PathVariable String id,
									@RequestParam("subjectactions") Optional<List<String>> subjectactions,
									@RequestParam("actionobjekts") Optional<List<String>> actionobjekts,
									@RequestParam("quantity") Integer quantity){
		if((!subjectactions.isPresent() || CollectionUtils.isEmpty(subjectactions.get()))
				|| (!actionobjekts.isPresent() || CollectionUtils.isEmpty(actionobjekts.get()))){
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		List<ThermVO> results = new ArrayList<>();
		List<SubjectAction> subjectActions = subjectActionService.findAll();
		List<ActionObjekt> actionObjekts = actionObjektService.findAll();
		HashMap<UUID, List<UUIDTriple>> mapThermsSubjects = new HashMap<>();
		subjectActions.forEach(sa -> {
			List<UUIDTriple> listTherms = mapThermsSubjects.get(sa.getActionId());
			if(CollectionUtils.isEmpty(listTherms)){
				listTherms = new ArrayList<>();
			}
			UUIDTriple triple = new UUIDTriple();
			triple.setFirst(sa.getSubjectId());
			listTherms.add(triple);
			mapThermsSubjects.put(sa.getActionId(),listTherms);
		});
		HashMap<UUID, List<UUIDTriple>> mapTherms = new HashMap<>();
		actionObjekts.forEach(ao -> {
			mapThermsSubjects.entrySet().forEach(uuidListEntry -> {
				List<UUIDTriple> listTherms = mapTherms.get(ao.getActionId());
				if(CollectionUtils.isEmpty(listTherms)){
					listTherms = new ArrayList<>();
				}
				for(UUIDTriple uuidTriple : uuidListEntry.getValue()){
					listTherms.add(new UUIDTriple(uuidTriple.getFirst(),ao.getObjektconnectionId(),ao.getObjektId()));
				}
				mapTherms.put(ao.getActionId(),listTherms);
			});
		});
		/*mapTherms.entrySet().stream().forEach(uuidListEntry -> {
			uuidListEntry.getValue().removeIf(uuidTriple -> (uuidTriple.getFirst() == null || uuidTriple.getThird() == null));
		});
		mapTherms.entrySet().removeIf(uuidListEntry -> CollectionUtils.isEmpty(uuidListEntry.getValue()));*/
		if(mapTherms.size() <= 0){
			return ResponseEntity.ok(results);
		}
		List<ThermVO> thermsToRandomize = new ArrayList<>();
		mapTherms.entrySet().forEach(uuidListEntry -> {
			uuidListEntry.getValue().forEach(uuidTriple -> {
				ThermVO therm = new ThermVO();
				therm.setActionId(uuidListEntry.getKey());
				therm.setSubjectId(uuidTriple.getFirst());
				therm.setObjektconnectionId(uuidTriple.getSecond());
				therm.setObjektId(uuidTriple.getThird());
				thermsToRandomize.add(therm);
			});
		});
		for(int i = quantity; i > 0 && thermsToRandomize.size() > 0; i--){
			ThermVO thermVO = null;
			int rand = 0;
			if(thermsToRandomize.size() > 1){
				Random r = new Random();
				int low = 0;
				int high = thermsToRandomize.size() - 1;
				rand = r.nextInt(high-low) + low;
				thermVO = thermsToRandomize.get(rand);
			}else{
				thermVO = thermsToRandomize.get(0);
				rand = 0;
			}

			thermVO.setSubject(subjectService.findById(thermVO.getSubjectId()).get());
			thermVO.setAction(actionService.findById(thermVO.getActionId()).get());
			if(thermVO.getObjektconnectionId() != null){
				thermVO.setObjektConnection(objektConnectionService.findById(thermVO.getObjektconnectionId()).get());
			}
			thermVO.setObjekt(objektService.findById(thermVO.getObjektId()).get());
			results.add(thermVO);
			thermsToRandomize.remove(rand);
		}
		return ResponseEntity.ok(results);
	}

	public class UUIDTriple{
		private UUID first;
		private UUID second;
		private UUID third;

		public UUIDTriple(){}

		public UUIDTriple(UUID first, UUID second, UUID third) {
			this();
			this.first = first;
			this.second = second;
			this.third = third;
		}

		public UUID getFirst() {
			return first;
		}

		public void setFirst(UUID first) {
			this.first = first;
		}

		public UUID getSecond() {
			return second;
		}

		public void setSecond(UUID second) {
			this.second = second;
		}

		public UUID getThird() {
			return third;
		}

		public void setThird(UUID third) {
			this.third = third;
		}
	}
}
