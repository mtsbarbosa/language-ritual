package net.papalegba.languageritual.vo;

import net.papalegba.languageritual.model.Action;
import net.papalegba.languageritual.model.Subject;
import net.papalegba.languageritual.model.SubjectAction;

public class SubjectActionResponseVO extends SubjectAction {
	private Subject subject;
	private Action action;

	public Subject getSubject() {
		return subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	public Action getAction() {
		return action;
	}

	public void setAction(Action action) {
		this.action = action;
	}
}
