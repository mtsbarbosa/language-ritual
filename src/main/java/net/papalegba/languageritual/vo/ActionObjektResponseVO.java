package net.papalegba.languageritual.vo;

import net.papalegba.languageritual.model.Action;
import net.papalegba.languageritual.model.ActionObjekt;
import net.papalegba.languageritual.model.Objekt;
import net.papalegba.languageritual.model.ObjektConnection;

public class ActionObjektResponseVO extends ActionObjekt {
	private Action action;
	private ObjektConnection objektConnection;
	private Objekt objekt;

	public Action getAction() {
		return action;
	}

	public void setAction(Action action) {
		this.action = action;
	}

	public ObjektConnection getObjektConnection() {
		return objektConnection;
	}

	public void setObjektConnection(ObjektConnection objektConnection) {
		this.objektConnection = objektConnection;
	}

	public Objekt getObjekt() {
		return objekt;
	}

	public void setObjekt(Objekt objekt) {
		this.objekt = objekt;
	}
}
