package net.papalegba.languageritual.vo;

import net.papalegba.languageritual.model.Action;
import net.papalegba.languageritual.model.Objekt;
import net.papalegba.languageritual.model.ObjektConnection;
import net.papalegba.languageritual.model.Subject;

import java.util.UUID;

public class ThermVO {
	private Subject subject;
	private Action action;
	private ObjektConnection objektConnection;
	private Objekt objekt;
	private UUID subjectId;
	private UUID actionId;
	private UUID objektconnectionId;
	private UUID objektId;

	public Subject getSubject() {
		return subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	public Action getAction() {
		return action;
	}

	public void setAction(Action action) {
		this.action = action;
	}

	public ObjektConnection getObjektConnection() {
		return objektConnection;
	}

	public void setObjektConnection(ObjektConnection objektConnection) {
		this.objektConnection = objektConnection;
	}

	public Objekt getObjekt() {
		return objekt;
	}

	public void setObjekt(Objekt objekt) {
		this.objekt = objekt;
	}

	public UUID getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(UUID subjectId) {
		this.subjectId = subjectId;
	}

	public UUID getActionId() {
		return actionId;
	}

	public void setActionId(UUID actionId) {
		this.actionId = actionId;
	}

	public UUID getObjektconnectionId() {
		return objektconnectionId;
	}

	public void setObjektconnectionId(UUID objektconnectionId) {
		this.objektconnectionId = objektconnectionId;
	}

	public UUID getObjektId() {
		return objektId;
	}

	public void setObjektId(UUID objektId) {
		this.objektId = objektId;
	}
}
