package net.papalegba.languageritual.config;

import net.papalegba.languageritual.interceptor.SessionInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.handler.MappedInterceptor;

@Configuration
public class SpringWebRestConfig implements WebMvcConfigurer {
	@Autowired
	private SessionInterceptor sessionInterceptor;

	@Bean
	public MappedInterceptor correlationIdMappedInterceptor() {
		return new MappedInterceptor(new String[] {"/**"}, new String[] {"/login"}, sessionInterceptor);
	}
}
