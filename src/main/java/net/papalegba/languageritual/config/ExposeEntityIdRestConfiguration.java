package net.papalegba.languageritual.config;

import net.papalegba.languageritual.model.Action;
import net.papalegba.languageritual.model.ActionObjekt;
import net.papalegba.languageritual.model.Language;
import net.papalegba.languageritual.model.Objekt;
import net.papalegba.languageritual.model.ObjektConnection;
import net.papalegba.languageritual.model.Subject;
import net.papalegba.languageritual.model.SubjectAction;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;

@Configuration
public class ExposeEntityIdRestConfiguration implements RepositoryRestConfigurer {

	@Override
	public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
		config.exposeIdsFor(Language.class, Action.class, ActionObjekt.class, Objekt.class, ObjektConnection.class, Subject.class, SubjectAction.class);
	}
}