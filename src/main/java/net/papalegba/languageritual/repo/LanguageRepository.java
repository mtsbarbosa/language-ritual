package net.papalegba.languageritual.repo;

import net.papalegba.languageritual.model.Language;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.UUID;

@RepositoryRestResource
public interface LanguageRepository extends CassandraRepository<Language, UUID> {

}
