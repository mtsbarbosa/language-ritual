package net.papalegba.languageritual.repo;

import net.papalegba.languageritual.model.SubjectAction;
import org.springframework.data.cassandra.repository.AllowFiltering;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;
import java.util.UUID;

@RepositoryRestResource
public interface SubjectActionRepository extends CassandraRepository<SubjectAction, UUID> {
	@AllowFiltering
	List<SubjectAction> findByLanguageId(UUID languageId);
}
