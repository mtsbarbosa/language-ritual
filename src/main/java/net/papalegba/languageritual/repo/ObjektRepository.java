package net.papalegba.languageritual.repo;

import net.papalegba.languageritual.model.Objekt;
import org.springframework.data.cassandra.repository.AllowFiltering;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;
import java.util.UUID;

@RepositoryRestResource
public interface ObjektRepository extends CassandraRepository<Objekt, UUID> {
	@AllowFiltering
	List<Objekt> findByLanguageId(UUID languageId);

	@AllowFiltering
	List<Objekt> findByTranslationContainingAndLanguageId(String translation, UUID languageId);
}
