package net.papalegba.languageritual.repo;

import net.papalegba.languageritual.model.Subject;
import org.springframework.data.cassandra.repository.AllowFiltering;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;
import java.util.UUID;

@RepositoryRestResource
public interface SubjectRepository extends CassandraRepository<Subject, UUID> {
	@AllowFiltering
	List<Subject> findByLanguageId(UUID languageId);

	@AllowFiltering
	List<Subject> findByTranslationContainingAndLanguageId(String translation, UUID languageId);
}
