package net.papalegba.languageritual.repo;

import net.papalegba.languageritual.model.Action;
import org.springframework.data.cassandra.repository.AllowFiltering;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;
import java.util.UUID;

@RepositoryRestResource
public interface ActionRepository extends CassandraRepository<Action, UUID> {
	@AllowFiltering
	List<Action> findByLanguageId(UUID languageId);

	@AllowFiltering
	List<Action> findByTranslationContainingAndLanguageId(String translation, UUID languageId);
}
