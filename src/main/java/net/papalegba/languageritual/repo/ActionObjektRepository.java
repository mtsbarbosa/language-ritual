package net.papalegba.languageritual.repo;

import net.papalegba.languageritual.model.ActionObjekt;
import org.springframework.data.cassandra.repository.AllowFiltering;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;
import java.util.UUID;

@RepositoryRestResource
public interface ActionObjektRepository extends CassandraRepository<ActionObjekt, UUID> {
	@AllowFiltering
	List<ActionObjekt> findByLanguageId(UUID languageId);
}
