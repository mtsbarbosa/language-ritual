package net.papalegba.languageritual.repo;

import net.papalegba.languageritual.model.ObjektConnection;
import org.springframework.data.cassandra.repository.AllowFiltering;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;
import java.util.UUID;

@RepositoryRestResource
public interface ObjektConnectionRepository extends CassandraRepository<ObjektConnection, UUID> {
	@AllowFiltering
	List<ObjektConnection> findByLanguageId(UUID languageId);

	@AllowFiltering
	List<ObjektConnection> findByTranslationContainingAndLanguageId(String translation, UUID languageId);
}
