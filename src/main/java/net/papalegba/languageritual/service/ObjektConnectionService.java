package net.papalegba.languageritual.service;

import net.papalegba.languageritual.model.ObjektConnection;
import net.papalegba.languageritual.repo.ObjektConnectionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class ObjektConnectionService {
	@Autowired
	private ObjektConnectionRepository objektConnectionRepository;

	public Optional<ObjektConnection> findById(UUID id){
		return objektConnectionRepository.findById(id);
	}

	public List<ObjektConnection> findByLanguageId(UUID languageId){
		return objektConnectionRepository.findByLanguageId(languageId);
	}

	public List<ObjektConnection> findByTranslationContainingAndLanguageId(String name, UUID languageId){
		return objektConnectionRepository.findByTranslationContainingAndLanguageId(name,languageId);
	}

	@Transactional(rollbackFor = Exception.class)
	public ObjektConnection insert(ObjektConnection objektConnection){
		return objektConnectionRepository.insert(objektConnection);
	}

	@Transactional(rollbackFor = Exception.class)
	public ObjektConnection save(ObjektConnection objektConnection){
		return objektConnectionRepository.save(objektConnection);
	}

	@Transactional(rollbackFor = Exception.class)
	public void deleteById(UUID id){
		objektConnectionRepository.deleteById(id);
	}
}
