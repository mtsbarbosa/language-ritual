package net.papalegba.languageritual.service;

import net.papalegba.languageritual.model.ActionObjekt;
import net.papalegba.languageritual.repo.ActionObjektRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Service
public class ActionObjektService {
	@Autowired
	private ActionObjektRepository actionObjektRepository;

	public List<ActionObjekt> findByLanguageId(UUID languageId){
		return actionObjektRepository.findByLanguageId(languageId);
	}

	public List<ActionObjekt> findAll(){
		return actionObjektRepository.findAll();
	}

	@Transactional(rollbackFor = Exception.class)
	public ActionObjekt insert(ActionObjekt actionObjekt){
		return actionObjektRepository.insert(actionObjekt);
	}

	@Transactional(rollbackFor = Exception.class)
	public ActionObjekt save(ActionObjekt actionObjekt){
		return actionObjektRepository.save(actionObjekt);
	}

	@Transactional(rollbackFor = Exception.class)
	public void deleteById(UUID id){
		actionObjektRepository.deleteById(id);
	}
}
