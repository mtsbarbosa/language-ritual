package net.papalegba.languageritual.service;

import net.papalegba.languageritual.model.Objekt;
import net.papalegba.languageritual.repo.ObjektRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class ObjektService {
	@Autowired
	private ObjektRepository objektRepository;

	public Optional<Objekt> findById(UUID id){
		return objektRepository.findById(id);
	}

	public List<Objekt> findByLanguageId(UUID languageId){
		return objektRepository.findByLanguageId(languageId);
	}

	public List<Objekt> findByTranslationContainingAndLanguageId(String name, UUID languageId){
		return objektRepository.findByTranslationContainingAndLanguageId(name,languageId);
	}

	@Transactional(rollbackFor = Exception.class)
	public Objekt insert(Objekt objekt){
		return objektRepository.insert(objekt);
	}

	@Transactional(rollbackFor = Exception.class)
	public Objekt save(Objekt objekt){
		return objektRepository.save(objekt);
	}

	@Transactional(rollbackFor = Exception.class)
	public void deleteById(UUID id){
		objektRepository.deleteById(id);
	}
}
