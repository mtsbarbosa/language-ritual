package net.papalegba.languageritual.service;

import net.papalegba.languageritual.model.Action;
import net.papalegba.languageritual.repo.ActionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class ActionService {
	@Autowired
	private ActionRepository actionRepository;

	public Optional<Action> findById(UUID id){
		return actionRepository.findById(id);
	}

	public List<Action> findByLanguageId(UUID languageId){
		return actionRepository.findByLanguageId(languageId);
	}

	public List<Action> findByTranslationContainingAndLanguageId(String name, UUID languageId){
		return actionRepository.findByTranslationContainingAndLanguageId(name,languageId);
	}

	@Transactional(rollbackFor = Exception.class)
	public Action insert(Action action){
		return actionRepository.insert(action);
	}

	@Transactional(rollbackFor = Exception.class)
	public Action save(Action action){
		return actionRepository.save(action);
	}

	@Transactional(rollbackFor = Exception.class)
	public void deleteById(UUID id){
		actionRepository.deleteById(id);
	}
}
