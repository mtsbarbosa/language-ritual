package net.papalegba.languageritual.service;

import net.papalegba.languageritual.model.SubjectAction;
import net.papalegba.languageritual.repo.SubjectActionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Service
public class SubjectActionService {
	@Autowired
	private SubjectActionRepository subjectActionRepository;

	public List<SubjectAction> findByLanguageId(UUID languageId){
		return subjectActionRepository.findByLanguageId(languageId);
	}

	public List<SubjectAction> findAll(){
		return subjectActionRepository.findAll();
	}

	@Transactional(rollbackFor = Exception.class)
	public SubjectAction insert(SubjectAction subjectAction){
		return subjectActionRepository.insert(subjectAction);
	}

	@Transactional(rollbackFor = Exception.class)
	public SubjectAction save(SubjectAction subjectAction){
		return subjectActionRepository.save(subjectAction);
	}

	@Transactional(rollbackFor = Exception.class)
	public void deleteById(UUID id){
		subjectActionRepository.deleteById(id);
	}
}
