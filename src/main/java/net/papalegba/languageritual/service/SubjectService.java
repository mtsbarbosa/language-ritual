package net.papalegba.languageritual.service;

import net.papalegba.languageritual.model.Subject;
import net.papalegba.languageritual.repo.SubjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class SubjectService {
	@Autowired
	private SubjectRepository subjectRepository;

	public Optional<Subject> findById(UUID id){
		return subjectRepository.findById(id);
	}

	public List<Subject> findByLanguageId(UUID languageId){
		return subjectRepository.findByLanguageId(languageId);
	}

	public List<Subject> findByTranslationContainingAndLanguageId(String name, UUID languageId){
		return subjectRepository.findByTranslationContainingAndLanguageId(name,languageId);
	}

	@Transactional(rollbackFor = Exception.class)
	public Subject insert(Subject subject){
		return subjectRepository.insert(subject);
	}

	@Transactional(rollbackFor = Exception.class)
	public Subject save(Subject subject){
		return subjectRepository.save(subject);
	}

	@Transactional(rollbackFor = Exception.class)
	public void deleteById(UUID id){
		subjectRepository.deleteById(id);
	}
}
