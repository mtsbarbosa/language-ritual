package net.papalegba.languageritual.model;

import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import java.util.UUID;

@Table
public class Language {
	@PrimaryKey
	private UUID id = UUID.randomUUID();

	@Column
	private String code;

	@Column
	private String name;

	public Language(UUID id) {
		this();
		this.id = id;
	}

	public Language(){
		if(id != null){
			id = UUID.randomUUID();
		}
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
