package net.papalegba.languageritual.model;

import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import java.util.UUID;

@Table
public class ActionObjekt {
	@PrimaryKey
	private UUID id;

	private UUID actionId;
	private UUID objektconnectionId;
	private UUID objektId;
	private UUID languageId;

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public UUID getActionId() {
		return actionId;
	}

	public void setActionId(UUID actionId) {
		this.actionId = actionId;
	}

	public UUID getObjektconnectionId() {
		return objektconnectionId;
	}

	public void setObjektconnectionId(UUID objektconnectionId) {
		this.objektconnectionId = objektconnectionId;
	}

	public UUID getObjektId() {
		return objektId;
	}

	public void setObjektId(UUID objektId) {
		this.objektId = objektId;
	}

	public UUID getLanguageId() {
		return languageId;
	}

	public void setLanguageId(UUID languageId) {
		this.languageId = languageId;
	}
}
