package net.papalegba.languageritual.util;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpSession;

public class SessionUtil {
	public static HttpSession getSession(){
		return getSession(false);
	}

	public static HttpSession getSession(boolean createSession){
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		return attr.getRequest().getSession(createSession);
	}

	public static boolean hasSession(){
		return getSession() != null;
	}
}
