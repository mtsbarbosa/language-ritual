package net.papalegba.languageritual.util;

import org.springframework.util.StringUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

public class CookieUtil {
	public static String getCookieValue(HttpServletRequest request, String cookieName){
		Cookie[] cookies = request.getCookies();
		if(cookies != null){
			for(Cookie cookie : cookies){
				if(cookie != null && !StringUtils.isEmpty(cookie.getName()) && cookie.getName().equalsIgnoreCase(cookieName)){
					return cookie.getValue();
				}
			}
		}
		return null;
	}
}
